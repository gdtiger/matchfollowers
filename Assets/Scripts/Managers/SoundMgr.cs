using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum SoundName
{
	MatchSuccess,
	MatchFail,
	StageClear,
	StageFail,
	FollowerSound,
	LikeSound,
	BoostPost,
}

public class SoundMgr : MonoBehaviour
{
	#region instance
	static SoundMgr instance = null;
	public static SoundMgr Instance
	{
		get
		{
			if (instance == null)
			{
				instance = FindObjectOfType<SoundMgr>();
			}
			return instance;
		}
	}
	#endregion
	public AudioSource bgSound;
	public AudioClip[] bgList;
	public AudioClip[] effectSoundList;
	public AudioClip[] peopleSoundList;
	public AudioClip ButtonClick;
	void Start()
	{
		if (bgSound != null)
		{
		BgSoundPlay(bgList[0]);
		}
	}
	public void SFXPlay(string sfxName,AudioClip clip,float soundSize)
	{
		if (GameManager.Instance.gameData.EffectSound)
		{
			GameObject go = new GameObject(sfxName + "Sound");
			AudioSource audiosource = go.AddComponent<AudioSource>();
			audiosource.clip = clip;
			audiosource.volume = soundSize;
			audiosource.Play();

			Destroy(go, clip.length);
		}
	}

	private void OnSceneLoaded(Scene arg0, LoadSceneMode arg1)
	{
		for (int i = 0; i < bgList.Length; i++)
		{
			if (arg0.name == bgList[i].name)
			{
				BgSoundPlay(bgList[0]);
			}
		}
	}

	public void BgSoundPlay(AudioClip clip)
	{
		if (GameManager.Instance.gameData.BGMSound)
		{
			bgSound.clip = clip;
			bgSound.loop = true;
			bgSound.volume = 1f;
			bgSound.Play();
		}
	}
}
