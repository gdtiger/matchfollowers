using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMgr : MonoBehaviour
{
	private static bool isIntro=true;
	public float delayTime, fadeTime;
	public GameObject IntroPanel;
	public GameObject MainPanel;

	void Awake()
	{
		if(isIntro)
		{
			StartCoroutine(DelayTime(delayTime,fadeTime));
		}
		else
		{
			IntroPanel.SetActive(false);
			MainPanel.SetActive(true);
		}
	}

	IEnumerator DelayTime(float time, float fade)
	{
		yield return new WaitForSeconds(time);
		IntroPanel.GetComponent<UI_FadeInOut>().fadeTime = fade;
		yield return new WaitForSeconds(fade);
		IntroPanel.SetActive(false);
		MainPanel.SetActive(true);
		isIntro = false;
	}

	public void GoStageScene()
	{
		SceneManager.LoadScene($"StageScene{GameManager.Instance.gameData.Posts}");
	}
}
