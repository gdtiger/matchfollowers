using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
	#region instance
	static GameManager instance = null;
	public static GameManager Instance
	{
		get
		{
			if (instance == null)
			{
				instance = FindObjectOfType<GameManager>();
			}
			return instance;
		}
	}
	#endregion
	[Header("DataSystem")]
	public DataMgr DataManager;
	public string PlayerSave = "MFPlayerSave";
	public string StageSave = "MFStageSave";
	public ImageData profileImageData;
	public ImageData postImageData;
	public ImageData commentImageData;
	public StageData stageData;
	public GameData gameData;
	[Space]
	[Header("GameSystem")]
	public GameObject introUI;

	private bool isIntro;
	private float timer;
	private int clickCount;
	private void Awake() 
	{
		var obj = FindObjectsOfType<GameManager>(); if (obj.Length == 1) 
		{
			DontDestroyOnLoad(gameObject); 
		}
		else 
		{
			Destroy(gameObject); 
		}
	}

	void Start()
	{
		gameData = DataManager.DataLoadText<GameData>(PlayerSave);
	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			clickCount++;
			if (!IsInvoking("DoubleClick"))
				Invoke("DoubleClick", 1.0f);
		}
		else if (clickCount == 2)
		{
			CancelInvoke("DoubleClick");
			Application.Quit();
		}
	}
	void DoubleClick()
	{
		clickCount = 0;
	}
}
