using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;

public class DataMgr : MonoBehaviour
{
    public void DataSaveText<T>(T data, string _fileName)
    {
        try
        {
            string json = JsonUtility.ToJson(data, true);
            if (json.Equals("{}"))
            {
                Debug.Log("json null");
                return;
            }
            string path = Application.persistentDataPath + "/" + _fileName + ".json";
            File.WriteAllText(path, json);
        }
        catch (FileNotFoundException e)
        {
            Debug.Log("The file was not found:" + e.Message);
        }
        catch (DirectoryNotFoundException e)
        {
            Debug.Log("The directory was not found: " + e.Message);
        }
        catch (IOException e)
        {
            Debug.Log("The file could not be opened:" + e.Message);
        }
    }


    public T DataLoadText<T>(string _fileName) where T : new()
    {
        try
        {
            string path = Application.persistentDataPath + "/" + _fileName + ".json";
            if (File.Exists(path))
            {
                string json = File.ReadAllText(path);
                T t = JsonUtility.FromJson<T>(json);
                return t;
            }
            else {
                Debug.Log("Missing file");
                DataSaveText(new T(), _fileName);
                DataLoadText<T>(_fileName);
                Debug.Log("Missing file Save");
                Debug.Log($"Missing file ReloadScene{SceneManager.GetActiveScene().buildIndex}");
            }
        }
        catch (FileNotFoundException e)
        {
            Debug.Log("The file was not found:" + e.Message);
        }
        catch (DirectoryNotFoundException e)
        {
            Debug.Log("The directory was not found: " + e.Message);
        }
        catch (IOException e)
        {
            Debug.Log("The file could not be opened:" + e.Message);
        }
        return default;
    }
}
