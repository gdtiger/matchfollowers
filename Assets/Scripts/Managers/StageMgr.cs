using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using GoogleMobileAds.Api;

public class StageMgr : MonoBehaviour
{

	#region instance
	static StageMgr instance = null;
	public static StageMgr Instance
	{
		get
		{
			if (instance == null)
			{
				instance = FindObjectOfType<StageMgr>();
			}
			return instance;
		}
	}
	#endregion


	[Header("스폰 시스템")]
	public Items[] prefabs;
	public float gameTimer;
	public int spawnIndex;
	[HideInInspector]
	public float spawnRangeX = 4f;
	[HideInInspector]
	public float spawnPosY = 0f;
	[HideInInspector]
	public float spawnRangeZ = 7.5f;
	[HideInInspector]
	public Match match;
	[Space]
	[Space]

	[Header("게임 시스템")]
	public List<Items> prefabCharator;
	public static int gameStage = 0;
	public float waitTime = 9;
	[HideInInspector]
	public bool isPeopleMoving;
	public bool startGame;
	[HideInInspector]
	public bool stopTimer;
	public GameObject particle;

	[Header("스코어 시스템")]
	public int followersValue = 1;

	[Header("매치 시스템")]
	public float destroyTime;
	[HideInInspector]
	public float doTime;

	[Header("콤보 시스템")]
	public float comboTimer;
	public float minComboTimer;
	public float downComboTimer;

	[Header("UI제어")]
	public GameObject AdPanel;
	public GameObject FailPanel;
	public GameObject ClearPanel;
	public GameObject TopStagePanel;
	public GameObject PlatStartPanel;
	public GameObject BottomStagePanel;
	public bool isPlay = true;
	public bool isFail = false;
	public bool isClear = false;
	public bool isTutorial = false;

	[Header("광고")]

	public static readonly string interstitial1Id = "ca-app-pub-1195551850458243/3678818220";
	public static readonly string reward1Id = "ca-app-pub-1195551850458243/6094046813";

	private InterstitialAd interstitial;
	private RewardedAd rewardedAd;


	void Start()
	{
		GameManager.Instance.gameData = GameManager.Instance.DataManager.DataLoadText<GameData>(GameManager.Instance.PlayerSave);
		OnClickInit();
		OnClickRequestInterstitial();
		OnClickRequestReward();
	}
	void FixedUpdate()
	{
		waitTime -= Time.deltaTime;
		if (waitTime <= 0 && !startGame)
		{
			waitTime = 0f;
			TopStagePanel.SetActive(true);
			BottomStagePanel.SetActive(true);
			isPeopleMoving = true;
			startGame = true;
		}
		if (startGame)
		{
			if (prefabCharator.Count == 0 && !stopTimer && isPlay)
			{
				AdPanel.SetActive(true);
				isPlay = false;
			}
			else if (prefabCharator.Count > 0 && stopTimer && isPlay)
			{
				FailPanel.SetActive(true);
				StageMgr.Instance.startGame = false;
				Time.timeScale = 0;
				SoundMgr.Instance.SFXPlay("GameClear", SoundMgr.Instance.effectSoundList[(int)SoundName.StageFail], 1f);
				isPlay = false;
			}
		}
	}
	public void RemoveObjects(int id)
	{
		for (int i = 0; i < prefabCharator.Count; i++)
		{
			if (prefabCharator[i].id == id)
			{
				Destroy(prefabCharator[i].gameObject, 0.5f);
				prefabCharator.RemoveAt(i);
				i--;
			}
		}
	}
	public void OnClickInit()
	{
		List<string> deviceIds = new List<string>();
		deviceIds.Add("F9C0BF933719D3AD84E61BE8C7BC375A");
		RequestConfiguration requestConfiguration = new RequestConfiguration
			.Builder()
			.SetTestDeviceIds(deviceIds)
			.build();
		MobileAds.SetRequestConfiguration(requestConfiguration);
		MobileAds.Initialize(initStatus => { });
	}
	public void OnClickRequestInterstitial()
	{
		if (interstitial != null)
		{
			interstitial.Destroy();
		}
		interstitial = new InterstitialAd(interstitial1Id);
		this.interstitial.OnAdLoaded += HandleOnAdLoaded;
		this.interstitial.OnAdOpening += HandleOnAdOpened;

		AdRequest request = new AdRequest.Builder().Build();
		interstitial.LoadAd(request);
	}

	public void HandleOnAdLoaded(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdLoaded event received");
	}

	public void HandleOnAdOpened(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleAdOpened event received");
	}
	public void OnClickInterstitial()
	{
		if (interstitial.IsLoaded())
		{
			interstitial.Show();
		}
	}
	public void OnClickRequestReward()
	{
		if (rewardedAd != null)
		{
			rewardedAd.Destroy();
		}
		this.rewardedAd = new RewardedAd(reward1Id);
		this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
		this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;

		this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
		AdRequest request = new AdRequest.Builder().Build();
		this.rewardedAd.LoadAd(request);
	}

	public void HandleRewardedAdLoaded(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdLoaded event received");
	}

	public void HandleRewardedAdOpening(object sender, EventArgs args)
	{
		MonoBehaviour.print("HandleRewardedAdOpening event received");
	}
	public void OnClickReward()
	{
		if (this.rewardedAd.IsLoaded())
		{
			this.rewardedAd.Show();
		}
	}

	public void HandleUserEarnedReward(object sender, Reward args)
	{
		string type = args.Type;
		double amount = args.Amount;
		if(isClear)
		{
			AdPanel.GetComponent<UI_Ad>().GetReward();
		}
		else if (isFail)
		{
			FailPanel.GetComponent<UI_Fail>().GetFailReward();
		}
	}
}
