using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI_StartPlat : MonoBehaviour
{
	public Text userName;
	public Text curPost;
	public Text followerIndex;
	public Image profileImage;
	public Image postImage;

	void Update()
	{
		profileImage.sprite = GameManager.Instance.profileImageData.GetImage(GameManager.Instance.gameData.profileImageID);
		postImage.sprite = GameManager.Instance.postImageData.GetImage(GameManager.Instance.gameData.Posts);
		userName.text = $"@{GameManager.Instance.gameData.UserName}";
		curPost.text = $"Post {(GameManager.Instance.gameData.Posts)+1}";
		followerIndex.text = $"{GameManager.Instance.stageData.Stage[GameManager.Instance.gameData.Posts]}";
		if (GameManager.Instance.gameData.Posts == 0)
		{
			curPost.text = "-";
		}
	}

	public void OnClickBack()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		gameObject.SetActive(false);
	}
	public void OnClickBoostPost()
	{
		SoundMgr.Instance.SFXPlay("Boost Post", SoundMgr.Instance.effectSoundList[(int)SoundName.BoostPost], 1f);
		StartCoroutine(coBoost());
	}

	IEnumerator coBoost()
	{
		yield return new WaitForSeconds(1f);
		SceneManager.LoadScene($"StageScene{GameManager.Instance.gameData.Posts}");
	}
}
