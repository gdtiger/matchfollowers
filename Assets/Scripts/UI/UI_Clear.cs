using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class UI_Clear : MonoBehaviour
{
	public Text userName;
	public Text postsText;
	public Text followersText;
	public Text likeByText;
	public Image profileImage;
	public Image postImage;

	void Start()
	{
		userName.text =$"@ {GameManager.Instance.gameData.UserName}";
		postsText.text = $"{GameManager.Instance.gameData.Posts-1}";
		profileImage.sprite = GameManager.Instance.profileImageData.GetImage(GameManager.Instance.gameData.profileImageID);
		postImage.sprite = GameManager.Instance.postImageData.GetImage(GameManager.Instance.gameData.Posts-1);
		likeByText.text = $"Like By <b>djqpwj</b> and <b>0 others</b>";
		StartCoroutine(Count(
			GameManager.Instance.gameData.FollowerScore, 
			GameManager.Instance.gameData.LikeScore[GameManager.Instance.gameData.Posts-1],
			0,
			0));
	}
	public void OnHome()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		Time.timeScale = 1f;
		SceneManager.LoadScene("MainScene");
	}
	public void OnNextPosts()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		GameManager.Instance.gameData.TotalScore += GameManager.Instance.gameData.FollowerScore;
		GameManager.Instance.gameData.FollowerScore = 0;
		GameManager.Instance.DataManager.DataSaveText<GameData>(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
		StageMgr.Instance.PlatStartPanel.SetActive(true);
	}

	IEnumerator Count(float target, float target2, float current, float current2)
	{
		SoundMgr.Instance.SFXPlay("FollowerSound", SoundMgr.Instance.effectSoundList[(int)SoundName.FollowerSound], 1f);
		float duration = 1f; 
		
		float offset = (target - current) / duration;
		while (current < target)
		{
			current += offset * Time.deltaTime;
			followersText.text = ((int)current).ToString();
			yield return null;
		}
		current = target;
		followersText.text = ((int)current).ToString();
		SoundMgr.Instance.SFXPlay("LikeSound", SoundMgr.Instance.effectSoundList[(int)SoundName.LikeSound], 1f);
		float duration2 = 1f;
		float offset2 = (target2 - current2) / duration2; 
		while (current2 < target2)
		{
			current2 += offset2 * Time.deltaTime;
			likeByText.text = $"Like By <b>djqpwj</b> and <b>{((int)current2).ToString()} others</b>";
			yield return null;
		}
		current2 = target2;
		likeByText.text = $"Like By <b>djqpwj</b> and <b>{((int)current2).ToString()} others</b>";
	}
}
