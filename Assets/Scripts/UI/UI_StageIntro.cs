using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_StageIntro : MonoBehaviour
{
    void Start()
    {
        SoundMgr.Instance.SFXPlay("Intro", SoundMgr.Instance.effectSoundList[(int)SoundName.MatchSuccess], 1f);
    }
}
