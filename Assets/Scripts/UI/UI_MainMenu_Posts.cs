using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI_MainMenu_Posts : MonoBehaviour
{
	public Transform parent;
	public Button startPost; 
	public Button picture;
	public List<Button> pictures;
	void Update()
	{
		CreatePosts();
	}
	public void CreatePosts()
	{
		if (parent.childCount - 1 < GameManager.Instance.gameData.Posts)
		{
			for (int i = parent.childCount - 1; i < GameManager.Instance.gameData.Posts; i++)
			{
				var post = Instantiate(picture, parent);
				post.transform.SetAsFirstSibling();
				post.image.sprite = GameManager.Instance.postImageData.GetImage(i);
				pictures.Add(post);
			}
			startPost.transform.SetSiblingIndex(0);
			Debug.Log($"parent.childCount - 1 : {parent.childCount - 1}, pictures.Count : {pictures.Count}");
		}
	}
}
