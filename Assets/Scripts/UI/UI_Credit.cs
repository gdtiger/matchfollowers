using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Credit : MonoBehaviour
{
	public void OnClickBack()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		gameObject.SetActive(false);
	}
}
