using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_MainMenu : MonoBehaviour
{
	public Text totalFollowersText;
	public Text totalPostsText;
	public Text UserName;
	public Image profileImage;
	public UI_Option optionUI;
	public UI_StartPlat platStartUI;
	public UI_Profile profileUI;
	public GameObject debugUI;
	private int totalFollowers;
	void Start()
	{
		totalFollowers = GameManager.Instance.gameData.TotalScore + GameManager.Instance.gameData.FollowerScore;
		GameManager.Instance.gameData.FollowerScore = 0;
		GameManager.Instance.gameData.TotalScore = totalFollowers;
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
		totalFollowersText.text = $"{GameManager.Instance.gameData.TotalScore}";
		totalPostsText.text = $"{(GameManager.Instance.gameData.Posts)}";
		if (GameManager.Instance.gameData.Posts == 0)
		{
			totalPostsText.text = "-";
		}
	}
	void Update()
	{
		UserName.text = $"@{GameManager.Instance.gameData.UserName}";
		profileImage.sprite = GameManager.Instance.profileImageData.GetImage(GameManager.Instance.gameData.profileImageID);

	}

	public void OnOption()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		optionUI.gameObject.SetActive(true);
	}
	public void OnStartPlat()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		platStartUI.gameObject.SetActive(true);
	}
	public void OnClickProfileUI()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		profileUI.gameObject.SetActive(true);
	}
	public void OnResetScore()
	{
		GameManager.Instance.gameData.FollowerScore = 0;
		GameManager.Instance.gameData.Posts = 0;
		GameManager.Instance.gameData.TotalScore = 0;
		totalFollowers = GameManager.Instance.gameData.TotalScore;
		for (int i = 0; i < GameManager.Instance.gameData.Posts; i++)
		{
			GameManager.Instance.gameData.LikeScore.Add(0);
		}
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
	}
	public void OnClickDebug()
	{
		debugUI.SetActive(true);
	}
}
