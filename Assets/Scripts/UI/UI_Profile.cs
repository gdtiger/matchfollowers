using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Profile : MonoBehaviour
{
	public Button curbutton;
	public Button[] Slots;
	public int viewSlot;
	void Start()
	{
		Slots[GameManager.Instance.gameData.profileImageID].transform.GetChild(0).gameObject.SetActive(true);
		for (int i = 0; i < Slots.Length; i++)
		{
			Slots[i].gameObject.SetActive(i < viewSlot);
			Slots[i].GetComponent<Image>().sprite =
				GameManager.Instance.profileImageData.GetImage(i);

			int index = i;
			Slots[index].onClick.AddListener(() => this.OnClickProfile(index));
		}
	}

	public void OnClickBack()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		gameObject.SetActive(false);
	}
	public void OnClickProfile(int index)
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		curbutton = Slots[index];
		Slots[index].transform.GetChild(0).gameObject.SetActive(true);
		for (int i = 0; i < Slots.Length; i++)
		{
			if (Slots[i] != curbutton)
			{
				Slots[i].transform.GetChild(0).gameObject.SetActive(false);
			}
		}
		GameManager.Instance.gameData.profileImageID = index;
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
		GameManager.Instance.DataManager.DataLoadText<GameData>(GameManager.Instance.PlayerSave);
	}

	public void profileImageCount()
	{
		if (GameManager.Instance.gameData.TotalScore < 1000)
		{
			viewSlot = 1;
		}
		else if (GameManager.Instance.gameData.TotalScore < 2000 &&GameManager.Instance.gameData.TotalScore >= 1000)
		{
			viewSlot = 2;
		}
		else if (GameManager.Instance.gameData.TotalScore < 3000 && GameManager.Instance.gameData.TotalScore >= 2000)
		{
			viewSlot = 3;
		}
		else if (GameManager.Instance.gameData.TotalScore < 4000 && GameManager.Instance.gameData.TotalScore >= 3000)
		{
			viewSlot = 4;
		}
		else if (GameManager.Instance.gameData.TotalScore < 5000 && GameManager.Instance.gameData.TotalScore >= 4000)
		{
			viewSlot = 5;
		}
		else if (GameManager.Instance.gameData.TotalScore < 6000 && GameManager.Instance.gameData.TotalScore >= 5000)
		{
			viewSlot = 6;
		}
		else if (GameManager.Instance.gameData.TotalScore < 7000 && GameManager.Instance.gameData.TotalScore >= 6000)
		{
			viewSlot = 7;
		}
		else if (GameManager.Instance.gameData.TotalScore < 8000 && GameManager.Instance.gameData.TotalScore >= 7000)
		{
			viewSlot = 8;
		}
		else if (GameManager.Instance.gameData.TotalScore >= 8000)
		{
			viewSlot = 9;
		}
	}
}
