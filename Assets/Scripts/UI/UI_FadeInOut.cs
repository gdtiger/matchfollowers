using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum Fade
{
	In,
	Out
}

public class UI_FadeInOut : MonoBehaviour
{
	public float fadeTime;
	public Fade fade;

	private float time;
	private bool isfadeStop;
	// Update is called once per frame
	void Update()
	{
		if (!isfadeStop)
		{
			if (fade == Fade.In)
			{
				if (time <= fadeTime)
				{
					GetComponent<Image>().color = new Color(1, 1, 1,1 - time / fadeTime);

				}
				else
				{
					gameObject.SetActive(false);
					isfadeStop = true;
				}
			}
			if (fade == Fade.Out)
			{
				if (time <= fadeTime)
				{
					GetComponent<Image>().color = new Color(1, 1, 1, 1 - time / fadeTime);

				}
				else
				{
					isfadeStop = true;
				}
			}
			time += Time.deltaTime;
		}
	}
}
