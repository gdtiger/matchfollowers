using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Tutorial : MonoBehaviour
{
	public float ActiveTime;

	void Start()
	{
		StartCoroutine(coStart(ActiveTime));
	}

	IEnumerator coStart(float x)
	{
		yield return new WaitForSeconds(x);
		gameObject.transform.GetChild(0).gameObject.SetActive(true);
	}

	public void OnClosedTutorial()
	{
		gameObject.SetActive(false);
	}
}
