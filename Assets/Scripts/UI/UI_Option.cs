using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;


public class UI_Option : MonoBehaviour
{
	public Image BGMoff;
	public Image EffectSoundOff;
	public Text userNameText;
	public InputField changeUserName;
	public GameObject Credit;

	void Start()
	{
		if (GameManager.Instance.gameData.BGMSound)BGMoff.gameObject.SetActive(false);
		else BGMoff.gameObject.SetActive(true);
		if (GameManager.Instance.gameData.EffectSound) EffectSoundOff.gameObject.SetActive(false);
		else EffectSoundOff.gameObject.SetActive(true);
		changeUserName.characterLimit = 12;
		changeUserName.onValueChanged.AddListener((word) => changeUserName.text = Regex.Replace(word, @"[^a-zA-Z0-9_]", ""));
		changeUserName.text = GameManager.Instance.gameData.UserName;
	}
	void Update()
	{
		
	}

	public void OnClickUserNameEdit()
	{
		GameManager.Instance.gameData.UserName = changeUserName.text;
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData,GameManager.Instance.PlayerSave);
		GameManager.Instance.DataManager.DataLoadText<GameData>(GameManager.Instance.PlayerSave);
		changeUserName.text = changeUserName.text;
	}
	public void OnClickCredit()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		Credit.SetActive(true);
	}
	public void OnClickExit()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
		Application.Quit();
	}
	public void OnClickMusic()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		if (GameManager.Instance.gameData.BGMSound)
		{
			BGMoff.gameObject.SetActive(true);
			GameManager.Instance.gameData.BGMSound = false;
			GameManager.Instance.DataManager.DataSaveText(
				GameManager.Instance.gameData,
				GameManager.Instance.PlayerSave);
			GameManager.Instance.DataManager.DataLoadText<GameData>(
				GameManager.Instance.PlayerSave);
		}
		else
		{
			BGMoff.gameObject.SetActive(false);
			GameManager.Instance.gameData.BGMSound = true;
			GameManager.Instance.DataManager.DataSaveText(
				GameManager.Instance.gameData,
				GameManager.Instance.PlayerSave);
			GameManager.Instance.DataManager.DataLoadText<GameData>(
				GameManager.Instance.PlayerSave);
		}
	}
	public void OnclickEffect()
	{
		if (GameManager.Instance.gameData.EffectSound)
		{
			EffectSoundOff.gameObject.SetActive(true);
			GameManager.Instance.gameData.EffectSound = false;
			GameManager.Instance.DataManager.DataSaveText(
				GameManager.Instance.gameData,
				GameManager.Instance.PlayerSave);
			GameManager.Instance.DataManager.DataLoadText<GameData>(
				GameManager.Instance.PlayerSave);
		}
		else
		{
			EffectSoundOff.gameObject.SetActive(false);
			GameManager.Instance.gameData.EffectSound = true;
			GameManager.Instance.DataManager.DataSaveText(
				GameManager.Instance.gameData,
				GameManager.Instance.PlayerSave);
			GameManager.Instance.DataManager.DataLoadText<GameData>(
				GameManager.Instance.PlayerSave);
		}
	}
	public void OnClickBack()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		gameObject.SetActive(false);
	}
}
