using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Pause : MonoBehaviour
{
	public Text stageCount;

	void Start()
	{
		stageCount.text = $"Posts {GameManager.Instance.gameData.Posts}";
	}
	public void OnReStart()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		StageMgr.Instance.startGame = true;
		Time.timeScale = 1f;
		gameObject.SetActive(false);
	}
	public void OnHome()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		Time.timeScale = 1f;
		SceneManager.LoadScene("MainScene");
	}
	public void OnRetry()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		Time.timeScale = 1f;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
}
