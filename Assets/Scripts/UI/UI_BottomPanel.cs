using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_BottomPanel : MonoBehaviour
{
    public Text PostCount;
	public Text needCount;

	void Start()
	{
		if (GameManager.Instance.gameData.Posts == 0)
		{
			PostCount.text = "Tutorial Stage!";
		}
		else
		PostCount.text = $"Post {(GameManager.Instance.gameData.Posts)+1}";
	}
	void Update()
    {
		needCount.text = $"{StageMgr.Instance.prefabCharator.Count/2}";

	}
}
