using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_TopPanel : MonoBehaviour
{
	public GameObject PausePanel;
	public Image profileImage;
	public Text userNameText;
	public Text followerText;
	public int followerScore;

	void Start()
	{
		followerScore = 0;
		userNameText.text = $"@ {GameManager.Instance.gameData.UserName}";
		profileImage.sprite = GameManager.Instance.profileImageData.GetImage(GameManager.Instance.gameData.profileImageID);
	}
	void Update()
	{
		followerText.text = $"{followerScore}";
	}
	public void OnPause()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		StageMgr.Instance.startGame = false;
		Time.timeScale = 0f;
		PausePanel.SetActive(true);
	}
}