using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Fail : MonoBehaviour
{
	public Timer timer;
	public Button Addbutton;
	void Update()
	{
		if (!Addbutton.interactable)
		{
			Addbutton.transform.GetChild(0).GetComponent<Text>().text = "<size=40>Start over again</size>";
			Addbutton.transform.GetChild(1).gameObject.SetActive(false);
			Addbutton.transform.GetChild(2).gameObject.SetActive(false);
		}
	}

	public void OnAddTime()
	{
		//�����ִ°�

		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		StageMgr.Instance.isFail = true;
		StageMgr.Instance.OnClickReward();
		StageMgr.Instance.OnClickRequestReward();
	}
	public void OnHome()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		Time.timeScale = 1f;
		GameManager.Instance.gameData.FollowerScore = 0;
		SceneManager.LoadScene("MainScene");
	}
	public void OnRetry()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
		Time.timeScale = 1f;
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}
	public void GetFailReward()
	{
		timer.timeLeft += 60f;
		StageMgr.Instance.stopTimer = false;
		StageMgr.Instance.gameTimer += 60f;
		StageMgr.Instance.startGame = true;
		StageMgr.Instance.isPlay = true;
		Time.timeScale = 1f;
		gameObject.SetActive(false);
	}
}
