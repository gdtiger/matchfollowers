using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_Ad : MonoBehaviour
{
	public GameObject ClearParticle;
    public GameObject ClearPanel;
    public Text adText;
    public Image movePoint;
    public Button ADButton;
    public float speed;
    public float speedDecrease;
    public float moveDistance;
    public Vector2 minMaxMovePoint;

    public List<RectTransform> targets;
    public List<float> point;

    bool goingLeft;
    bool Running;
    float time = 0;
    float curSpeed;
    public int result;
    void Start()
    {
        GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
        StartCoroutine(CoClear());
        adText.text = "Get X5";
		for (int i = 0; i < targets.Count; i++)
		{
            point.Add(targets[i].position.x + 0.2165f);
		}
    }
    public void Run() {
        Running = true;
        time = 0;
        curSpeed = goingLeft ? -speed : speed;
        result = 5;
    }

    public void GetResult() {

		for (int i = 0; i < point.Count; i++)
		{
			if (movePoint.rectTransform.position.x < point[i])
			{
                result = i;
                
                break;
            }
        }
    }
	void Update()
	{
        if (result != 5)
		{
            ADButton.interactable = true;
            if (result == 0 || result == 4)
			{
                adText.text = $"Get X2\n<size=30>{GameManager.Instance.gameData.FollowerScore * 2}</size>";
            }
            else if (result == 1 || result == 3)
            {
                adText.text = $"Get X4\n<size=30>{GameManager.Instance.gameData.FollowerScore * 4}</size>";
            }
            else if (result == 2)
			{
                adText.text = $"Get X5\n<size=30>{GameManager.Instance.gameData.FollowerScore * 5}</size>";
            }
        }
        if (Running)
		{
            if (movePoint.rectTransform.position.x < minMaxMovePoint.x) {
                goingLeft = false;
                curSpeed = -curSpeed;
            }else
            if( movePoint.rectTransform.position.x > minMaxMovePoint.y)
            {
                goingLeft = true;
                curSpeed = -curSpeed;
            }

            if (goingLeft)
            {
                curSpeed += speedDecrease * Time.deltaTime * Random.Range(1, 10);
                if (curSpeed > 0)
                {
                    Running = false;
                    GetResult();
                }
            }
            else {
                curSpeed -= speedDecrease * Time.deltaTime * Random.Range(1,10);
                if (curSpeed < 0)
                {
                    Running = false;
                    GetResult();
                }
            }
            var temp = movePoint.rectTransform.position;
            temp.x += curSpeed;
            movePoint.rectTransform.position = temp;
        }
		
    }

	IEnumerator CoClear()
    {
        ClearParticle.SetActive(true);
        StageMgr.Instance.startGame = false;
        GameManager.Instance.gameData.Posts++;
        GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
        yield return new WaitForSecondsRealtime(1f);
        Run();
        SoundMgr.Instance.SFXPlay("GameClear", SoundMgr.Instance.effectSoundList[(int)SoundName.StageClear], 1f);
        ClearPanel.SetActive(true);
        yield return new WaitForSecondsRealtime(5f);
        Debug.Log($"Score : {GameManager.Instance.gameData.FollowerScore}, TotalScore : {GameManager.Instance.gameData.TotalScore}");
        StageMgr.Instance.isPeopleMoving = false;
    }

    public void OnNoThank()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
        GameManager.Instance.gameData.LikeScore.Add(GameManager.Instance.gameData.FollowerScore);
        gameObject.SetActive(false);
        StageMgr.Instance.ClearPanel.SetActive(true);
        SoundMgr.Instance.bgSound.Stop();
	}
    public void OnClickAD()
	{
		SoundMgr.Instance.SFXPlay("Button", SoundMgr.Instance.ButtonClick, 1f);
        StageMgr.Instance.isClear = true;
        StageMgr.Instance.OnClickReward();
        SoundMgr.Instance.bgSound.Stop();
        StageMgr.Instance.OnClickRequestReward();
    }

    public void GetReward()
	{
        if (result == 0 || result == 4)
        {

            GameManager.Instance.gameData.LikeScore.Add(GameManager.Instance.gameData.FollowerScore * 2);
            GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
        }
        else if (result == 1 || result == 3)
        {
            GameManager.Instance.gameData.LikeScore.Add(GameManager.Instance.gameData.FollowerScore * 4);
            GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
        }
        else if (result == 2)
        {
            GameManager.Instance.gameData.LikeScore.Add(GameManager.Instance.gameData.FollowerScore * 5);
            GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
        }
        gameObject.SetActive(false);
        StageMgr.Instance.ClearPanel.SetActive(true);
    }
}
