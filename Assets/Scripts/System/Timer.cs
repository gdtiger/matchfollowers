using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float timeLeft;
    float redTime;
    Image timerBar;
    void Start()
    {
        timeLeft = StageMgr.Instance.gameTimer;
        redTime = timeLeft * 0.3f;
        timerBar = GetComponent<Image>();
    }

    void Update()
    {
		if (timeLeft > 0 && StageMgr.Instance.startGame)
		{
			if (timeLeft > redTime)
			{
				timerBar.color = new Color(140 / 255f, 215 / 255f, 70 / 255f);
			}
			if (timeLeft < redTime)
			{
				timerBar.color = Color.red;
			}
			if(StageMgr.Instance.isPlay&&!StageMgr.Instance.isTutorial)
			{
				timeLeft -= Time.deltaTime;
			}
			timerBar.fillAmount = timeLeft / StageMgr.Instance.gameTimer;
		}
		if (timeLeft <= 0)
		{
			StageMgr.Instance.stopTimer = true;
		}
	}
}
