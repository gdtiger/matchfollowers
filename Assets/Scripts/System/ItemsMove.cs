using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ItemsMove : MonoBehaviour
{
	public float movingRadius;
	public float minTimer;
	public float maxTimer;
	private Vector3 randomDirection;
	private Vector3 finalPos;

	private Vector3 oldPos;
	private Vector3 curPos;

	private Animator anim;
	private Transform target;
	private NavMeshAgent nav;
	private float timer;
	private float movingTimer = 5f;

	void OnEnable()
	{
		nav = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator>();
		timer = movingTimer;
	}
	void Start()
	{
		oldPos = transform.position;
	}

	void Update()
	{
		if (StageMgr.Instance.isPeopleMoving)
		{
			curPos = transform.position;
			var distance = curPos - oldPos;
			var velocity = (distance / Time.deltaTime).magnitude;
			oldPos = curPos;
			movingTimer = Random.Range(minTimer, maxTimer);
			randomDirection.y = transform.position.y;
			anim.SetFloat("speed", velocity);
			timer += Time.deltaTime;
			if (timer >= movingTimer)
			{
				Vector3 newPos = RandomNavLocation(transform.position, movingRadius, -1);
				if (nav.enabled)
				{
					nav.SetDestination(newPos);
				}
				timer = 0;
			}
		}
	}

	public Vector3 RandomNavLocation(Vector3 origin, float dist, int layermask)
	{
		randomDirection = Random.insideUnitSphere * dist;
		randomDirection += origin;
		NavMeshHit navHit;
		finalPos = Vector3.one;
		if (NavMesh.SamplePosition(randomDirection, out navHit, dist, layermask))
		{
			finalPos = navHit.position;
		}
		return finalPos;
	}
}
