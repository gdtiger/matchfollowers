using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComboSystem : MonoBehaviour
{
	public int currentCombo;
	private float currentComboTimer;
	public float comboTime;
	public int countCombo;
	public ParticleSystem comboPaticle;

	private Text text;
	private bool isCombo = false;
	private float rateTime = 1f;

	public bool IsCombo 
	{ 
		get => isCombo;
		set {
			isCombo = value;
			var prevCombo = isCombo;
			currentComboTimer = 0f;
			if (!isCombo)
			{
				StageMgr.Instance.comboTimer = comboTime;
				currentCombo = 0;
				countCombo = 0;
				rateTime = 1;
				comboPaticle.Stop();
				return;
			}
			if (!prevCombo && isCombo)
			{
				currentCombo = 1;
				Debug.Log("?");
			}
			else if (prevCombo && isCombo)
			{
				comboPaticle.Play();
				++currentCombo;
				if (StageMgr.Instance.comboTimer > StageMgr.Instance.minComboTimer)
				{
					StageMgr.Instance.comboTimer -= StageMgr.Instance.downComboTimer;
					++rateTime;
					var emission = comboPaticle.emission;
					emission.rateOverTime = rateTime;
				}
				else if (StageMgr.Instance.comboTimer < StageMgr.Instance.minComboTimer)
				{
					StageMgr.Instance.comboTimer = StageMgr.Instance.minComboTimer;
				}
			}
		} 
	}

	void Start()
	{
		text = GetComponent<Text>();
		currentCombo = 0;
		comboTime = StageMgr.Instance.comboTimer;
	}

	void Update()
	{
		if (IsCombo)
		{
			currentComboTimer += Time.deltaTime;
			if (IsCombo && currentComboTimer >= StageMgr.Instance.comboTimer)
			{
				IsCombo = false;
			}
		}
		text.text = $"Combo x {currentCombo}";
	}
}
