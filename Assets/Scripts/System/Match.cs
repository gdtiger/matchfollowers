using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using DG.Tweening;

public class Match : MonoBehaviour
{
	public List<GameObject> PlaceObjects = new List<GameObject>();
	public ComboSystem combo;
	public GameObject PointA;
	public GameObject PointB;
	public GameObject PointC;
	public StageMgr gameManager;
	public UI_TopPanel topUI;

	private Transform parent;
	public Image commentPrefabs;
	private int ScoreCount
	{
		get
		{
			if (combo.currentCombo == 0)
			{
				return 10;
			}
			else
				return (combo.currentCombo + 1) * 7;
		}
	}
	void Awake()
	{
		combo.GetComponent<ComboSystem>();
		parent = topUI.transform.GetChild(1);
	}
	void OnTriggerEnter(Collider other)
	{
		GameObject temp = other.gameObject;
		if (!other.GetComponent<Items>().itemEffect)
		{
			SetObject(temp);
		}

	}
	public void SetObject(GameObject target)
	{
		if (PlaceObjects.Count == 0 && PlaceObjects.Count != 1)
		{
			ObjectAction(target,PointA);
		}
		else if (target.transform.name.Contains(PlaceObjects[0].name))
		{
			ObjectAction(target,PointB);
			Tweening();
			PlaceObjects.Clear();
			gameManager.RemoveObjects(target.GetComponent<Items>().id);
			CreateComment();
		}
		else
		{
			target.transform.localScale = Vector3.one;
			target.GetComponent<Rigidbody>().velocity = 
				new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(0.5f, 1f), Random.Range(1f, 1.5f)) * 300 * Time.deltaTime;
			SoundMgr.Instance.SFXPlay("MatchFail", SoundMgr.Instance.effectSoundList[(int)SoundName.MatchFail], 1f);
		}
	}

	private void ObjectAction(GameObject target, GameObject location)
	{
		target.GetComponent<Animator>().SetBool("isCatch", false);
		target.transform.position = location.transform.position;
		target.transform.rotation = location.transform.rotation;
		target.GetComponent<Items>().itemEffect = true;
		target.GetComponent<Rigidbody>().useGravity = false;
		target.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
		target.GetComponent<BoxCollider>().isTrigger = true;
		target.GetComponent<NavMeshAgent>().enabled = false;
		target.transform.localScale =new Vector3(0.75f, 0.75f, 0.75f);
		PlaceObjects.Add(target);
	}
	void Tweening()
	{
		PlaceObjects[0].transform.DOMove(PointC.transform.position, StageMgr.Instance.doTime, false).SetEase(Ease.InQuad);
		PlaceObjects[1].transform.DOMove(PointC.transform.position, StageMgr.Instance.doTime, false).SetEase(Ease.InQuad)
			.OnComplete(() =>
			{
				SoundMgr.Instance.SFXPlay("MatchSuccess", SoundMgr.Instance.effectSoundList[(int)SoundName.MatchSuccess], 1f);
				PlaceObjects[1].SetActive(false);
			});
	}
	void OnTriggerExit(Collider other)
	{
		if (PlaceObjects.Contains(other.gameObject))
		{
			PlaceObjects.Remove(other.gameObject);
		}
	}
	void CreateComment()
	{
		GameManager.Instance.gameData.FollowerScore = topUI.followerScore += ScoreCount;
		combo.IsCombo = true;
		if (parent.childCount <= 1)
		{
			var comment = Instantiate(commentPrefabs, parent);
			comment.transform.SetAsLastSibling();
			comment.sprite = GameManager.Instance.commentImageData.GetImage(Random.Range(0, GameManager.Instance.commentImageData.images.Count));
		}
		else if (parent.childCount > 1)
		{
			parent.GetChild(0).GetComponent<Image>().DOColor(new Color(1, 1, 1, 0), 1f);
			Destroy(parent.GetChild(0).gameObject,1f);
			var comment = Instantiate(commentPrefabs, parent);
			comment.transform.SetAsLastSibling();
			comment.sprite = GameManager.Instance.commentImageData.GetImage(Random.Range(0, GameManager.Instance.commentImageData.images.Count));
		}
	}
}