using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Items : MonoBehaviour
{
	[HideInInspector]
	public bool itemEffect = false;
	public int id;
	public Match match;

	private Vector3 screenPoint;
	private Vector3 offset;
	private Rigidbody rigid;
	private NavMeshAgent nav;
	private Animator anim;
	void Start()
	{
		anim = GetComponent<Animator>();
		rigid = GetComponent<Rigidbody>();
		nav = GetComponent<NavMeshAgent>();
		
	}
	void OnMouseDown()
	{
		if (StageMgr.Instance.startGame)
		{
			anim.SetBool("isCatch", true);
			anim.speed = 1.2f;
			SoundMgr.Instance.SFXPlay("sound", SoundMgr.Instance.peopleSoundList[Random.Range(0, SoundMgr.Instance.peopleSoundList.Length)], Random.Range(0.1f, 0.5f));
			transform.rotation = Quaternion.Euler(0, 180, 0);
			transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
			nav.enabled = false;
			rigid.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
			itemEffect = false;
			rigid.useGravity = false;
			screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
			offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
		}
	}

	void OnMouseDrag()
	{
		if (StageMgr.Instance.startGame)
		{
			Vector3 mousePosition = new Vector3(Input.mousePosition.x,
		Input.mousePosition.y, 6f);
			var objPosition = Camera.main.ScreenToWorldPoint(mousePosition);
			transform.position = objPosition;
		}
	}
	void OnMouseUp()
	{
		if (StageMgr.Instance.startGame)
		{
			int layerMask = (-1) - (1 << LayerMask.NameToLayer("People"));
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast(ray, out hit, 1000, layerMask))
			{
				transform.position = hit.point;
			}
			rigid.useGravity = true;
			rigid.isKinematic = false;
			anim.enabled = true;
			anim.SetBool("isCatch", false);
		}
	}
}
