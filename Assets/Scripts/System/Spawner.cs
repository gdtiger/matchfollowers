using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;

public class Spawner : MonoBehaviour
{
	public StageMgr gm;
	public Transform parent;
    void Start()
    {
		StartCoroutine(coCreate());
	}

	IEnumerator coCreate()
	{
		yield return new WaitForSeconds(StageMgr.Instance.waitTime-5.5f);
		CreatePrefab();
	}
	public void CreatePrefab()
	{
		List<Items> gmPrefabs = StageMgr.Instance.prefabs.ToList();
		for (int i = 0; i < StageMgr.Instance.spawnIndex; i++)
		{
			Vector3 spawnPos = new Vector3(
				transform.position.x + Random.Range(-StageMgr.Instance.spawnRangeX, StageMgr.Instance.spawnRangeX),
				StageMgr.Instance.spawnPosY, 
				transform.position.z + Random.Range(-StageMgr.Instance.spawnRangeZ+3, StageMgr.Instance.spawnRangeZ));
			int index = Random.Range(0, gmPrefabs.Count);
			var people1 = Instantiate(gmPrefabs[index], spawnPos, Quaternion.Euler(0, 180, 0), parent);
			people1.GetComponent<Items>().match = gm.match;
			spawnPos = new Vector3(
				transform.position.x + Random.Range(-StageMgr.Instance.spawnRangeX, StageMgr.Instance.spawnRangeX),
				StageMgr.Instance.spawnPosY,
				transform.position.z + Random.Range(-StageMgr.Instance.spawnRangeZ, StageMgr.Instance.spawnRangeZ));
			var people2 = Instantiate(gmPrefabs[index], spawnPos, Quaternion.Euler(0, 180, 0), parent);
			people2.GetComponent<Items>().match = gm.match;
			gmPrefabs.RemoveAt(index);
			people1.id = people2.id = i;
			StageMgr.Instance.prefabCharator.Add(people1);
			StageMgr.Instance.prefabCharator.Add(people2);
		}
	}
}
