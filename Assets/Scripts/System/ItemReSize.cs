using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ItemReSize : MonoBehaviour
{
	public Match match;
	void OnTriggerEnter(Collider other)
	{
		other.transform.localScale = Vector3.one; 
		other.GetComponent<NavMeshAgent>().enabled = true;
		other.GetComponent<BoxCollider>().isTrigger = false;
		other.GetComponent<Items>().itemEffect = true;

	}
}
