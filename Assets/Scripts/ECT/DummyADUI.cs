using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DummyADUI : MonoBehaviour
{
    public Text text;
    private int score;
    public float timer = 0.05f;

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
		{
            timer = 0.05f;
            score += 13;
		}
        text.text = $"{3120 + score}";
    }
}
