using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroScene : MonoBehaviour
{
	public DataMgr DataManager;
	public string PlayerSave = "MFPlayerSave";
	public GameData gameData;
	private AudioSource IntroAudio;
	float timer;

	void Start()
	{
		IntroAudio = GetComponent<AudioSource>();
		StartCoroutine(coSound());
		gameData = DataManager.DataLoadText<GameData>(PlayerSave);
	}
	void Update()
    {
        if (timer < 5f)
		{
            timer += Time.deltaTime;
		}
        else if (timer >= 5f)
		{
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainScene");
		}
    }

	IEnumerator coSound()
	{
		yield return new WaitForSeconds(1.6f);
		IntroAudio.Play();
	}
}
