using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DummyAD : MonoBehaviour
{
    public float[] animationSpeeds;
    public Animator anime;
    public GameObject intro;
    void Start()
    {
        StartCoroutine(coAnimation(
            animationSpeeds[0], animationSpeeds[1], animationSpeeds[2],
            animationSpeeds[3], animationSpeeds[4], animationSpeeds[5]));
    }

    IEnumerator coAnimation(float first, float second, float third, float fourth, float fifth,float sixth)
	{
        yield return new WaitForSeconds(2f);
        Debug.Log("call coFirst"+Time.time);
        StartCoroutine(coFirst(first));
        yield return new WaitForSeconds(first);
        Debug.Log("call coSecond" + Time.time);
        StartCoroutine(coSecond());
        yield return new WaitForSeconds(second);
        Debug.Log("call coThird" + Time.time);
        StartCoroutine(coThird());
        yield return new WaitForSeconds(third);
        Debug.Log("call coFourth" + Time.time);
        StartCoroutine(coFourth(fourth));
        yield return new WaitForSeconds(fourth);
        Debug.Log("call coFifth" + Time.time);
        StartCoroutine(coFifth());
        yield return new WaitForSeconds(fifth);
        Debug.Log("call coSixth" + Time.time);
        StartCoroutine(coSixth(sixth));
        yield return null;
    }
    IEnumerator coFirst(float x)
	{
        anime.SetBool("isFirst", true);
        transform.DOMoveX(0, x, false);
        yield return null;
	}
    IEnumerator coSecond()
	{
        intro.SetActive(true);
        StopCoroutine("coFirst");
        anime.SetBool("isFirst", false);
        yield return new WaitForSeconds(0.5f);
		anime.SetBool("isSecond", true);
        yield return null;
	}
    IEnumerator coThird()
    {
        StopCoroutine("coSecond");
        anime.SetBool("isSecond", false);
        anime.SetBool("isThird", true);
        yield return null;
    }
    IEnumerator coFourth(float x)
    {
        StopCoroutine("coThird");
        anime.SetBool("isThird", false);
        anime.SetBool("isFourth", true);
        transform.DOMoveX(-0.6f, x, false);
        yield return null;
    }
    IEnumerator coFifth()
    {
        StopCoroutine("coFourth");
        anime.SetBool("isFourth", false);
        anime.SetBool("isFifth", true);
        SoundMgr.Instance.SFXPlay("sound", SoundMgr.Instance.peopleSoundList[Random.Range(0, SoundMgr.Instance.peopleSoundList.Length)], Random.Range(0.1f, 0.5f));
        yield return null;
    }
    IEnumerator coSixth(float x)
    {
        transform.DOMoveX(2, x, false).SetEase(Ease.InCirc);
        transform.DOMoveY(2, x, false).SetEase(Ease.InCirc);
        yield return null;
    }
}
