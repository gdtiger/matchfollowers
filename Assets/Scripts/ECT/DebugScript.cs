using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class DebugScript : MonoBehaviour
{
	public Text text;
	public void OnPostsCountEdit()
	{
		GameManager.Instance.gameData.LikeScore.Clear();
		GameManager.Instance.gameData.StageData.Clear();
		GameManager.Instance.gameData.Posts = int.Parse(text.text);
		for (int i = 0; i < int.Parse(text.text); i++)
		{
			GameManager.Instance.gameData.LikeScore.Add(0);
			GameManager.Instance.gameData.StageData.Add(0);
		}
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
	}
	public void OnFollowersCountEdit()
	{
		GameManager.Instance.gameData.TotalScore = int.Parse(text.text);
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
	}
	public void OnProfileImageIDCountEdit()
	{
		GameManager.Instance.gameData.profileImageID = int.Parse(text.text);
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
	}
	public void ExitDebug()
	{
		GameManager.Instance.DataManager.DataSaveText(GameManager.Instance.gameData, GameManager.Instance.PlayerSave);
		SceneManager.LoadScene("MainScene");
		gameObject.SetActive(false);
	}
}
