using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "StageData", menuName = "Data/Stage", order = 1)]
public class StageData : ScriptableObject
{
    public List<int> Stage;
    public int GetStage(int StageIndex)
    {
        return Stage[StageIndex];
    }
}