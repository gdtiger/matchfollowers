using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class GameData
{
	public bool BGMSound = true;
	public bool EffectSound = true;
	public int FollowerScore = 0;
	public int Posts = 0;
	public int TotalScore = 0;
	public string UserName = "UserName";
	public int profileImageID = 0;
	public List<int> StageData;
	public List<int> LikeScore;
}