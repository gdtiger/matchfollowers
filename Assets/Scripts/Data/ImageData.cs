using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ImageData", menuName = "Data/Image", order = 1)]
public class ImageData : ScriptableObject
{
    public List<Sprite> images;
    public Sprite GetImage(int imageID)
    {
        return images[imageID];
    }
}