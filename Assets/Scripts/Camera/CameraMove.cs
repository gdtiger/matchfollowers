using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
	public GameObject cameraMove;
	public GameObject mainCamera;
	public GameObject dummyPeople;
	public GameObject introUI;
	void Start()
	{
		mainCamera.transform.position = new Vector3(0, 1f, 3.75f);
		mainCamera.transform.rotation = Quaternion.Euler(-10, 0, 0);
		StartCoroutine(coCameraMove());
	}

	IEnumerator coCameraMove()
	{
		yield return new WaitForSeconds(0.5f);
		introUI.SetActive(true);
		yield return new WaitForSeconds(3.5f);
		introUI.SetActive(false);
		cameraMove.GetComponent<CPC_CameraPath>().enabled = true;
		dummyPeople.GetComponent<DissolveSphere>().enabled = true;
		yield return new WaitForSeconds(5f);
		mainCamera.transform.position = new Vector3(0, 7, -11.5f);
		mainCamera.transform.rotation = Quaternion.Euler(25,0,0);
		dummyPeople.SetActive(false);
	}
}
