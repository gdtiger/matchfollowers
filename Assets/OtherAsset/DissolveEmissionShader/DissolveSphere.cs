﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveSphere : MonoBehaviour {

    Material mat;
    public float dissolveTime = 5;
    public  float dissolveCur = 0;
    private void Start() {
        mat = GetComponent<Renderer>().material;
        dissolveCur = 0;
    }

    private void Update() {
       
        if (dissolveCur < dissolveTime)
        {
            dissolveCur += Time.deltaTime;
            mat.SetFloat("_DissolveAmount", dissolveCur / dissolveTime);
        }
        else {
            mat.SetFloat("_DissolveAmount", 1);
            enabled = false;
        }
  
    }
}